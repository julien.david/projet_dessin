CFLAGS = -ggdb3 -O0 --std=c99 -Wall -lm `sdl2-config --libs --cflags` -lSDL2_image 

FIGURES = src/figures/point.o 

all: main.o src/liste_figure.o src/couleur.o src/gestion_evenement.o $(FIGURES)
	gcc main.o src/liste_figure.o src/couleur.o src/gestion_evenement.o $(FIGURES) -o projet $(CFLAGS)

test_couleur: tests/test_couleur.o src/couleur.o
	gcc tests/test_couleur.o src/couleur.o -o tests/test_couleur
	./tests/test_couleur
	rm tests/test_couleur

test_point: tests/test_point.o src/figures/point.o src/couleur.o
	gcc tests/test_point.o src/figures/point.o src/couleur.o -o tests/test_point $(CFLAGS)
	./tests/test_point
	rm tests/test_point


test: test_couleur test_point
